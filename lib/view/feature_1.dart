import 'package:flutter/material.dart';

class FeatureScreenView1 extends StatelessWidget {
  const FeatureScreenView1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Feature Screen 1'),
        ),
        body: Center(
          child: ElevatedButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('DashBoard')),
        ));
  }
}
